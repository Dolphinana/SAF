# SAF

[SAF by drummyfish](https://codeberg.org/drummyfish/SAF).
DS port of SAF by Dolphinana.

*This is currently work-in-progress.*



# TODO:
- Eliminate screen flickering ()
- Implement loading/write functionality ()
- Make it run at 25 FPS instead of 30 FPS ()
- Implement audio ()



# How to compile (TODO)

The library for Nintendo DS can be install with devkitpro's pacman. [Get started here](https://devkitpro.org/wiki/Getting_Started)

The Makefile's license is not compatible with the license of this port and thus, cannot be included in this repository. But you can get the Makefile from the official devkitpro's example. Put the makefile in this repository, put all the .c files in the source directory, and make :)



## usage rights

*This section was copied straight from drummyfish's README.md, with tiny modification*

tl;dr: CC0 public domain, I hate intellectual property, do whatever you want

I (Dolphinana) have made this project completely myself from scratch. Everything in this repository is available under CC0 1.0 (public domain, https://creativecommons.org/publicdomain/zero/1.0/) + a waiver of all other IP rights (including patents and trademarks).

This project is made out of love and to be truly helpful to everyone, not for any self interest. I want it to forever stay completely in the public domain, not owned by anyone.

This is not mandatory but please consider supporting free software and free culture by using free licenses and/or waivers.

If you'd like to support me or just read something about me and my projects, visit my site: https://dolphinana.neocities.org

You can also choose to use this under the following waiver which is here to just ensure more legal safety and waiving of additional IP such as patents:

The intent of this waiver is to ensure that this work will never be encumbered by any exclusive intellectual property rights and will always be in the public domain world-wide, i.e. not putting any restrictions on its use.

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.
